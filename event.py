# used by view to communicate with model


class Event:
    pass


class Board(Event):
    def __init__(self, noPlayer):
        self.playerID = noPlayer
        self.cellSelected = None

    def setSelected(self, row, col):
        self.cellSelected = (row, col)


class GameState(Event):
    def __init__(self, state):
        self.state = state


class Player(Event):
    def __init__(self, id, tocreate=False):
        self.id = id
        self.tocreate = tocreate

