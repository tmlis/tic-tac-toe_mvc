from enum import Enum, unique, auto, IntEnum

@unique # to ensure each value is used only once in the enumeration
class GameState(Enum):  # starts with 1
    NONE = auto()
    NEW = auto()
    END = auto()
    WIN = auto()
    TIE = auto()
    INCOMPLETE = auto()


class Menu(Enum):
    pass


class Cell(Enum):
    X = auto()
    Y = auto()
