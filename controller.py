from model import Game
from ui.view import ViewCreator

# TODO
'''
multiple languages, probably xml files with different namings of view's components
computer player
change chararray to char.array
'''


def setup_env(development_state=False):
    if development_state:
        import logging
        logging.basicConfig(level=logging.INFO, format='%(name)s %(levelname)s %(threadName)s %(message)s')
        logging.info('controller setup')
    return


setup_env(development_state=True)

game = Game()
# ui = ViewCreator.tkinterView()
ui = ViewCreator.qtView()

game.notify.addObserver(ui.update)
ui.action.addObserver(game.update)


game.start()
ui.start()
