from observer import Observer, Observable
from event import Board as BoardChange, \
    GameState as GameStateChange
from notification import Board as BoardSignal, \
    GameState as GameStateSignal, \
    Player as PlayerSignal
from threading import Thread
import numpy as np
import enums
from player import Player
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class Game(Thread):
    def __init__(self):
        Thread.__init__(self)
        self._logic = Logic(self)
        # notify GUI
        self.notify = Game.Notify(self)
        # react on GUI
        self.update = Game.Update(self)

        self._state = set()

    def run(self):
        logger.info('started Game Thread')

    class Notify(Observable):
        def __init__(self, outer):
            self.outer = outer
            Observable.__init__(self)

        def notifyObservers(self, arg):
            self.setChanged()
            Observable.notifyObservers(self, arg)

    class Update(Observer):
        def __init__(self, outer):
            self.outer = outer

        def update(self, observable, event):
            if isinstance(event, BoardChange):
                notification = self.outer._logic.board_event(event)
                if notification is not None:
                    self.outer.notifyUi(notification)
            elif isinstance(event, GameStateChange):
                self.outer.notifyUi(self.outer._logic.gamestate_event(event))

    def notifyUi(self, notifications):
        """Takes notifications tuples as argument. Notifies User Interface"""
        for n in notifications:
            self.notify.notifyObservers(n)

    def new(self):
        logger.info('new')
        self._logic.addPlayer(1)
        self._logic.addPlayer(2)
        self._logic.nextTurn()

    def end(self):
        self._logic = Logic(self)

    def setState(self, state):
        """sets state and returns its value"""
        if state is enums.GameState.NEW:
            if not self.isState(state):
                self._addState(enums.GameState.NEW)
                self._removeState(enums.GameState.END)
                return state
        elif state is enums.GameState.END:
            if not self.isState(state) and self.isState(enums.GameState.NEW):  # if game was started
                self._addState(state)
                self._removeState(enums.GameState.NEW)
                return state
        return enums.GameState.NONE

    def _addState(self, state):
        self._state.add(state)

    def _removeState(self, state):
        try:
            self._state.remove(state)
        except:
            pass

    def isState(self, state):
        """True if state is in self._state"""
        if state in self._state:
            return True
        return False


class Logic:
    def __init__(self, outer):
        self.outer = outer
        self._players = []
        self._current = None
        self._board = np.chararray((3, 3), unicode=True)
        self._board[:] = ''

    def addPlayer(self, id):
        if id == 1:
            self._players.append(Player(1, 'x'))
            return True
        elif id == 2:
            self._players.append(Player(2, 'o'))
            return True
        return False

    def getPlayer(self, id):
        for player in self._players:
            if player.id == id:
                return player
        return None

    def nextTurn(self):
        """Sets next player as current, and returns it."""
        if not self._players:  # no players
            return
        if self._current is None:
            self._current = self._players[0]
        else:
            index = self._players.index(self._current)
            index += 1
            if index > len(self._players) - 1:
                index = 0
            self._current = self._players[index]

        return self._current

    def currentPlayer(self):
        """returns current player"""
        if not self._current:  # this check should not be necessary # to do
            if self._players:
                self._current = self._players[0]
        return self._current

    def cellisEmpty(self, row, col):  # returns True when to be filled
        """Returns True if cell is empty, otherwise False."""
        row -= 1
        col -= 1
        if not self.cellExist(row, col):
            return False
        # if not empty return False
        if self._board[row][col] != '':
            return False
        return True

    def cellFill(self, row, col):
        """Set sign of a cell."""
        row -= 1
        col -= 1
        if not self.cellExist(row, col):
            return
        self._board[row][col] = self.currentPlayer().sign  # fill cell

    def cellExist(self, row, col):
        """checks if board contains cell"""
        if row < 0 or row > self._board.shape[0]-1 or \
                col < 0 or col > self._board.shape[1]-1:
            return False
        return True

    def getResult(self, board, cell):
        """Returns win, tie, or incomplete.
               Checks only """
        rows, cols = board.shape
        r, c = cell
        r -= 1
        c -= 1
        playersign = board[r][c]
        # check vertical win
        result = enums.GameState.WIN
        for i in range(rows):
            if board[i][c] != playersign:
                result = enums.GameState.INCOMPLETE
        if result == enums.GameState.WIN:
            return result
        # check horizontal win
        result = enums.GameState.WIN
        for i in range(cols):
            if board[r][i] != playersign:
                result = enums.GameState.INCOMPLETE
        if result == enums.GameState.WIN:
            return result

        # check for a tie
        result = enums.GameState.TIE
        for i in board.tolist():
            for j in i:
                if j is '':
                    result = enums.GameState.INCOMPLETE
        if result == enums.GameState.TIE:
            return result

        # check if board is a square, otherwise no chance for diagonal win
        if rows != cols:
            return enums.GameState.INCOMPLETE

        # check main diagonal win \
        result = enums.GameState.WIN
        for i in range(cols):
            if board[i][i] != playersign:
                result = enums.GameState.INCOMPLETE
        if result == enums.GameState.WIN:
            return result

        # check anti diagonal win /
        result = enums.GameState.WIN
        for i in range(cols-1, -1, -1):
            if board[cols-1-i][i] != playersign:
                result = enums.GameState.INCOMPLETE
        if result == enums.GameState.WIN:
            return result

        return enums.GameState.INCOMPLETE

    def getBestMove(self):
        """returns the best possible move, not sure if should not be in Player class"""
        # to do
        pass


    # event handlers

    def board_event(self, event):
        """Handles cell click on a board, and returns appropriate notification tuple, if no notification returns None."""
        if event.cellSelected is not None:
            if self.cellisEmpty(*event.cellSelected):  # selected cell is empty
                self.cellFill(*event.cellSelected)
                gameresult = self.getResult(self._board, event.cellSelected)
                nfn = (BoardSignal(event.cellSelected, self.currentPlayer().id, True),  # cell is to be filled
                       GameStateSignal(gameresult))  # game result
                if gameresult == enums.GameState.INCOMPLETE:
                    nfn = nfn + (PlayerSignal(id=self.nextTurn().id,move=True),)  # next turn, player info

                return nfn
        else:
            return BoardSignal(*event.cellSelected, False),
        return None,

    def gamestate_event(self, event):
        """New; end; start game etc."""
        nfn = ()
        state = self.outer.setState(event.state)
        if state == enums.GameState.NEW:
            self.outer.new()
            nfn = (GameStateSignal(state),  # new game
                   PlayerSignal(id=self.getPlayer(1).id, create=True),  # add player 1
                   PlayerSignal(id=self.getPlayer(2).id, create=True),  # add player 2
                   PlayerSignal(id=self.getPlayer(1).id, move=True))  # set player 1 turn
        elif state == enums.GameState.END:
            self.outer.end()
            nfn = (GameStateSignal(state),)  # end game
        return nfn

