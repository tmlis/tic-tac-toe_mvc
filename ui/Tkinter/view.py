from ..base.view import BaseView
import tkinter as tk
from notification import Board as BoardChange, \
    GameState as GameStateChange, \
    Player as PlayerChange
from observer import Observable, Observer
from .board import BoardView, MenuView, ResultView, PlayerView
import enums
from player import Player
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class View(BaseView):
    def __init__(self):
        self.root = tk.Tk()
        self.menu = MenuView(self)
        self._boardview = None
        self._resultview = None
        self._playerinfoView = None
        self.menu.pack()
        self._currentplayer = None
        self.players = []

        self.action = self.Action(self)
        self.update = self.Update(self)

    def start(self):
        self.root.mainloop()

    class Action(Observable):
        def __init__(self, outer):
            self.outer = outer
            Observable.__init__(self)

        def notifyObservers(self, arg):
            self.setChanged()
            Observable.notifyObservers(self, arg)

    class Update(Observer):
        def __init__(self, outer):
            self.outer = outer
            self._boardview = None

        def update(self, observable, notification):
            if isinstance(notification, BoardChange):
                if notification.agreement:
                    self.outer._boardview.updateCell(notification.cell)
            elif isinstance(notification, GameStateChange):
                if notification.state == enums.GameState.NEW:
                    self.outer.createBoard()
                elif notification.state == enums.GameState.END:
                    self.outer.deleteWidget(self.outer._boardview)
                    self.outer.deleteWidget(self.outer._resultview)
                    self.outer.deleteWidget(self.outer._playerinfoView)
                elif notification.state == enums.GameState.TIE or notification.state == enums.GameState.WIN:
                    self.outer.handleResult(notification.state)

            elif isinstance(notification, PlayerChange):
                if notification.create is True:
                    if notification.id == 1:
                        self.outer.players.append(dict([('id', notification.id), ('player', Player(notification.id, 'x'))]))
                    elif notification.id == 2:
                        self.outer.players.append(dict([('id', notification.id), ('player', Player(notification.id, 'o'))]))
                    else: return
                    logger.info(f"created player [{self.outer.getPlayer(notification.id).id}:'{self.outer.getPlayer(notification.id).sign}']")

                elif notification.move is True:
                    self.outer.handlePlayer(notification.id)

    def createBoard(self):
        if self.isWidget(self._boardview):
            self.deleteWidget(self._boardview)

        self._boardview = BoardView(self)
        self._boardview.pack()

    def handleResult(self, result):
        self.deleteWidget(self._resultview)
        if result == enums.GameState.TIE:
            self.deleteWidget(self._playerinfoView)
            self._resultview = ResultView(self, 'Tie')
            self._resultview.pack()
        elif result == enums.GameState.WIN:
            self.deleteWidget(self._playerinfoView)
            self._resultview = ResultView(self, "Winner is '" + self.currentPlayer().sign+"'")
            self._resultview.pack()

    def currentPlayer(self): return self._currentplayer

    def getPlayer(self, id):
        for item in self.players:
            if item['id'] == id:
                return item['player']

    """Invoked when logic sets Next player's turn. Arg: id of next player"""
    def handlePlayer(self, player_id):
        self._currentplayer = self.getPlayer(player_id)
        self.setPlayerInfo(self._currentplayer.sign + ' turn')
        logger.info(f"turn of {self._currentplayer.sign}, unlocking board")
        self._boardview.lock(False)

    def deleteWidget(self, widget):
        if self.isWidget(widget):
            widget.destroy()

    def isWidget(self, widget):
        if widget is None:
            return False
        elif widget.destroyed():
            return False
        return True

    def setPlayerInfo(self, text):
        if not self.isWidget(self._playerinfoView):
            self._playerinfoView = PlayerView(self, text)
            self._playerinfoView.pack()
        self._playerinfoView.setInfo(text)


