from ..base.board import BaseBoardView, BaseMenuView, BasePlayerView, BaseResultView
from event import Board as BoardSignal, \
    GameState as GameStateSignal
from tkinter import Button, Frame, Label
import enums


class _View(Frame):
    """Base class of all views."""
    def __init__(self):
        Frame.__init__(self)

    def destroyed(self):
        if self._tclCommands is None:
            return True
        else:
            return False


class BoardView(BaseBoardView, _View):
    """Tic tac toe board 3x3."""
    def __init__(self, mainview):
        _View.__init__(self)
        self._main = mainview
        self._lock = False
        self._cells = []
        self._rows = 3
        self._cols = 3
        no = 0
        for i in range(self._rows):
            for j in range(self._cols):
                b = Button(self, text="OK", command=lambda r=no: self.cellClicked(r))
                b.grid(row=i, column=j)
                # b = Button(self.root, text="OK")
                # b.pack()
                # b.bind('<Button-1>', lambda r=no: self.cellClicked(r))
                self._cells.append(b)
                no += 1

    def cellClicked(self, no):
        if self._lock:
            return
        action = BoardSignal(self._main.currentPlayer().id)
        action.setSelected(*self.findCellpos(no))
        self._main.action.notifyObservers(action)

    def lock(self, _bool=True):
        self._lock = _bool
        # to do
        # disable board, player should be unable to click on a cell

    def updateCell(self, pos):
        self.lock()
        pattern = self._main.currentPlayer().sign
        self.findCell(*pos).configure(text=pattern)

    def findCell(self, row, col):
        ccc=(row-1)*self._cols + col-1
        return self._cells[(row-1)*self._cols + col-1]

    def findCellpos(self, n):
        row = int(n / self._cols) + 1
        col = n % self._cols + 1
        return row, col


class MenuView(BaseMenuView, _View):
    """Main menu view."""
    def __init__(self, mainview):
        _View.__init__(self)
        self._main = mainview
        li=["new game", "end game"]
        for j in range(len(li)):
            b = Button(self, text=li[j], command=lambda r=j: self.btnClicked(r+1))
            b.pack()

    def btnClicked(self, no):
        action = GameStateSignal(enums.GameState(no+1))
        # action.setSelected(*self.findCellpos(no))
        self._main.action.notifyObservers(action)


class ResultView(BaseResultView, _View):
    """Result of a game. Information."""
    def __init__(self, mainview, result):
        _View.__init__(self)
        # result can be : owin, xwin, tie
        self.frame = Frame()
        b = Label(self, text=result)
        b.pack()


class PlayerView(BasePlayerView, _View):
    def __init__(self, mainview, info):
        _View.__init__(self)
        self._info = Label(self, text=info)
        self._info.pack()

    def setInfo(self, text):
        self._info.config(text=text)

