class ViewCreator:
    @staticmethod
    def tkinterView():
        try:
            from ui.Tkinter.view import View
            return View()
        except ImportError:
            print("Tkinter view import error")

    @staticmethod
    def qtView():
        try:
            from ui.PyQt.view import View
            return View()
        except ImportError:
            print("PyQt view import error")