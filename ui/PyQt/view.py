from PyQt5 import QtWidgets
from notification import Board as BoardChange, \
    GameState as GameStateChange, \
    Player as PlayerChange
from observer import Observable, Observer
from .board import BoardView, MenuView, ResultView, PlayerView
import enums
from player import Player
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class View(QtWidgets.QWidget):
    def __init__(self):
        self.app = QtWidgets.QApplication([])
        super().__init__()
        self.mainlayout = QtWidgets.QVBoxLayout()
        self.menu = MenuView(self)
        self.mainlayout.addWidget(self.menu)
        self.setLayout(self.mainlayout)
        self._boardview = None
        self._resultview = None
        self._playerinfoView = None
        self._currentplayer = None
        self.players = []

        self.action = self.Action(self)
        self.update = self.Update(self)

    def start(self):
        self.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.show()
        self.app.exec()

    class Action(Observable):
        def __init__(self, outer):
            self.outer = outer
            Observable.__init__(self)

        def notifyObservers(self, arg):
            self.setChanged()
            Observable.notifyObservers(self, arg)

    class Update(Observer):
        def __init__(self, outer):
            self.outer = outer
            self._boardview = None

        def update(self, observable, notification):
            if isinstance(notification, BoardChange):
                if notification.agreement:
                    self.outer._boardview.updateCell(notification.cell)
            elif isinstance(notification, GameStateChange):
                if notification.state == enums.GameState.NEW:
                    self.outer.createBoard()
                elif notification.state == enums.GameState.END:
                    self.outer._boardview = self.outer.deleteWidget(self.outer._boardview)
                    self.outer._resultview = self.outer.deleteWidget(self.outer._resultview)
                    self.outer._playerinfoView = self.outer.deleteWidget(self.outer._playerinfoView)
                elif notification.state == enums.GameState.TIE or notification.state == enums.GameState.WIN:
                    self.outer.handleResult(notification.state)

            elif isinstance(notification, PlayerChange):
                if notification.create is True:
                    if notification.id == 1:
                        self.outer.players.append(dict([('id', notification.id), ('player', Player(notification.id, 'x'))]))
                    elif notification.id == 2:
                        self.outer.players.append(dict([('id', notification.id), ('player', Player(notification.id, 'o'))]))
                    else: return
                    logger.info(f"created player [{self.outer.getPlayer(notification.id).id}:'{self.outer.getPlayer(notification.id).sign}']")

                elif notification.move is True:
                    self.outer.handlePlayer(notification.id)

    def createBoard(self):
        self._boardview = BoardView(self)
        self.mainlayout.addWidget(self._boardview)

    def handleResult(self, result):
        self._resultview = self.deleteWidget(self._resultview)
        if result == enums.GameState.TIE:
            self._resultview = ResultView(self, 'Tie')
            self.mainlayout.addWidget(self._resultview)
        elif result == enums.GameState.WIN:
            self._resultview = ResultView(self, "Winner is '" + self.currentPlayer().sign + "'")
            self.mainlayout.addWidget(self._resultview)
        else: return
        self._playerinfoView = self.deleteWidget(self._playerinfoView)

    def currentPlayer(self): return self._currentplayer

    def getPlayer(self, id):
        for item in self.players:
            if item['id'] == id:
                return item['player']

    def handlePlayer(self, player_id):
        self._currentplayer = self.getPlayer(player_id)
        self.setPlayerInfo(self._currentplayer.sign + ' turn')
        logger.info(f"turn of {self._currentplayer.sign}, unlocking board")
        self._boardview.lock(False)

    def deleteWidget(self, widget):
        if self.isWidget(widget):
            widget.hide()
            del widget
        return None

    def isWidget(self, widget):
        return False if widget is None else True

    def setPlayerInfo(self, text):
        logger.info(f"setplayernfo {self.isWidget(self._playerinfoView)}")
        if not self.isWidget(self._playerinfoView):
            self._playerinfoView = PlayerView(self, text)
            self.mainlayout.addWidget(self._playerinfoView)
        self._playerinfoView.setInfo(text)