from PyQt5 import QtWidgets
from PyQt5 import QtCore
from event import Board as BoardSignal, \
    GameState as GameStateSignal
import enums


class MenuView(QtWidgets.QWidget):
    """Main menu view."""
    def __init__(self, mainview):
        super().__init__()
        self._main = mainview
        li = ["new game", "end game"]
        vlayout = QtWidgets.QVBoxLayout()
        mapper = QtCore.QSignalMapper(self)
        for j in range(len(li)):
            b = QtWidgets.QPushButton(li[j], self)
            mapper.setMapping(b, j+1)
            b.clicked.connect(mapper.map)
            vlayout.addWidget(b)
        mapper.mapped[int].connect(self.btnClicked)
        self.setLayout(vlayout)

    def btnClicked(self, no):
        action = GameStateSignal(enums.GameState(no+1))
        self._main.action.notifyObservers(action)


class BoardView(QtWidgets.QWidget):
    """Tic tac toe board 3x3."""
    def __init__(self, mainview):
        super().__init__()
        self._main = mainview
        self._lock = False
        self._cells = []
        self._rows = 3
        self._cols = 3
        no = 0
        boxlayout = QtWidgets.QGridLayout()
        mapper = QtCore.QSignalMapper(self)
        for i in range(self._rows):
            for j in range(self._cols):
                b = QtWidgets.QPushButton("OK", self)
                mapper.setMapping(b, no)
                b.clicked.connect(mapper.map)
                boxlayout.addWidget(b, i, j)
                self._cells.append(b)
                no += 1
        mapper.mapped[int].connect(self.cellClicked)
        self.setLayout(boxlayout)

    def cellClicked(self, no):
        if self._lock:
            return
        action = BoardSignal(self._main.currentPlayer().id)
        action.setSelected(*self.findCellpos(no))
        self._main.action.notifyObservers(action)

    def lock(self, _bool=True):
        self._lock = _bool
        # TODO
        # disable board, player should be unable to click on a cell

    def updateCell(self, pos):
        self.lock()
        pattern = self._main.currentPlayer().sign
        self.findCell(*pos).setText(pattern)

    def findCell(self, row, col):
        ccc=(row-1)*self._cols + col-1
        return self._cells[(row-1)*self._cols + col-1]

    def findCellpos(self, n):
        row = int(n / self._cols) + 1
        col = n % self._cols + 1
        return row, col


class ResultView(QtWidgets.QLabel):
    """Result of a game. Information."""
    def __init__(self, mainview, result):
        super().__init__()
        self.setText(result)
        # result can be : owin, xwin, tie


class PlayerView(QtWidgets.QWidget):
    def __init__(self, mainview, info):
        super().__init__()
        vlayout = QtWidgets.QVBoxLayout()
        self._info = QtWidgets.QLabel(info, self)
        vlayout.addWidget(self._info)
        self.setLayout(vlayout)

    def setInfo(self, text):
        self._info.setText(text)