from abc import ABC, abstractmethod


class BaseBoardView(ABC):
    @abstractmethod
    def __init__(self, mainview): pass

    @abstractmethod
    def cellClicked(self, no): pass

    @abstractmethod
    def lock(self, _bool=True): pass

    @abstractmethod
    def updateCell(self, pos): pass

    @abstractmethod
    def findCell(self, row, col): pass

    @abstractmethod
    def findCellpos(self, n): pass


class BaseMenuView(ABC):
    @abstractmethod
    def __init__(self, mainview): pass

    @abstractmethod
    def btnClicked(self, no): pass


class BaseResultView(ABC):
    @abstractmethod
    def __init__(self, mainview, result): pass


class BasePlayerView(ABC):
    @abstractmethod
    def __init__(self, mainview, info): pass

    @abstractmethod
    def setInfo(self, text): pass