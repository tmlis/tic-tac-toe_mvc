from abc import ABC, abstractmethod


class BaseView(ABC):
    @abstractmethod
    def __init__(self): pass

    @abstractmethod
    def start(self): pass

    @abstractmethod
    def createBoard(self): pass

    @abstractmethod
    def handleResult(self, result): pass

    @abstractmethod
    def currentPlayer(self): pass

    @abstractmethod
    def getPlayer(self, id): pass

    @abstractmethod
    def handlePlayer(self, player_id): pass

    @abstractmethod
    def deleteWidget(self, widget): pass

    @abstractmethod
    def isWidget(self, widget): pass

    @abstractmethod
    def setPlayerInfo(self, text): pass
