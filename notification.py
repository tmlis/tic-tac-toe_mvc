# used by model to communicate with view


class Notification:
    pass


class Board(Notification):
    def __init__(self, cell, id, agreement):
        self.cell = cell
        self.id = id  # player who chose cell
        self.agreement = agreement  # True if has to be changed


class GameState(Notification):
    def __init__(self, state):
        self.state = state


class Player(Notification):
    def __init__(self, id, create=False, move=False):
        """ id: defines player
            create: player created
            move: player is allowed to make move """
        self.id = id
        self.create = create
        self.move = move

